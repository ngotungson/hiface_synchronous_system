import React from "react";
import DatePicker from './date_picker/DatePicker';
import { Button } from 'react-bootstrap';
import { MDBContainer, MDBRow, MDBCol } from 'mdbreact';
import axios from 'axios';
import './hf_export_report.css';    

class ExportReport extends React.Component {
    state = {
        startDate: '',
        endDate: '',
        exportStatus: ''
    }
    click = () => {
        this.setState({exportStatus: 'Exporting....'})
        console.log(this.props.userInfoFromParent)
        let temp = document.getElementById('myFile');
        // console.log(temp)
        let formData = new FormData();
        formData.append('file', temp.files[0])
        formData.append('start', this.state.startDate + ':00')
        formData.append('end', this.state.endDate + ':00' )
        formData.append('serverIP', this.props.userInfoFromParent.serverIP)
        formData.append('username', this.props.userInfoFromParent.username)
        formData.append('password', this.props.userInfoFromParent.password)

        // /attendance/
        fetch('/attendance/', {
            method: 'post',
            body: formData
        })
        .then(response => {
            this.setState({exportStatus: ''})
            response.blob().then(blob => {
                let url = window.URL.createObjectURL(blob);
                let a = document.createElement('a');
                a.href = url;
                // create file name
                let today = new Date();
                let dd = String(today.getDate()).padStart(2, '0');
                let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                let yyyy = today.getFullYear();

                today = mm + '/' + dd + '/' + yyyy;
                let fileName = 'Report-' + today + '.xlsx';
                a.download = fileName;
                a.click();
            });
        })
        .catch((error) => {
            console.error('Error:', error);
        });

        // console.log(formData)
    }

    handleDate = (dateInfo) => {
        this.setState({
            startDate: dateInfo.startDate,
            endDate: dateInfo.endDate
        })
        console.log(this.state)
    }

    config = event =>  {
        axios.get('/information/')
        .then(res => {
          this.setState({...res.data});
          // document.getElementById('configBtn').removeAttribute("onClick");
        });
      }
    
      componentDidMount() {
        this.config()
      }

    render() {
        return (
            
            <MDBContainer>
            <MDBRow>
              <MDBCol md="6">
                <form >
                    <label className="grey-text">Date Range</label>
                    <DatePicker id='datepicker' onChooseDate={this.handleDate}/>
                    <label className="grey-text">Custom File upload</label>
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">Upload</span>
                            </div>
                            <div className="custom-file">
                                <input type='file' id='myFile' ></input>
                            </div>
                        </div>
                    </div>
                    <div className="text-center mt-4">
                        <Button variant='success' onClick={this.click} disabled>EXPORT</Button>
                        <p style={{color:'#4CAF50'}}>{this.state.exportStatus}</p>
                    </div>
                </form>
              </MDBCol>
            </MDBRow>
          </MDBContainer>
        );
    }
}

export default ExportReport;