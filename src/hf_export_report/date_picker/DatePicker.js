import React from "react";
import { FormControl } from "react-bootstrap";
import moment from "moment";
import DateTimeRangeContainer from "react-advanced-datetimerange-picker";


class DatePicker extends React.Component {

    constructor(props) {
        super(props);
        let start = moment(new Date(2020, 2, 20, 0, 0, 0, 0));
        let end = moment(start)
          .add(5, "days")
          .subtract(1, "second");
        this.state = {
          start: start,
          end: end
        };
    
        this.onClick = this.onClick.bind(this);
        this.applyCallback = this.applyCallback.bind(this);
      }

    
    
      applyCallback(startDate, endDate) {
        console.log("Apply Callback");
        console.log(startDate.format("DD-MM-YYYY hh:mm A"));
        console.log(endDate.format("DD-MM-YYYY hh:mm A"));
        this.setState({
          start: startDate,
          end: endDate
        });
        this.props.onChooseDate({
            startDate: startDate.format("DD/MM/YYYY HH:mm"),
            endDate: endDate.format("DD/MM/YYYY HH:mm")
        });  
      }
    
      rangeCallback(index, value) {
        console.log(index, value);
      }
    
      onClick() {
        let newStart = moment(this.state.start).subtract(3, "days");
        // console.log("On Click Callback");
        // console.log(newStart.format("DD-MM-YYYY HH:mm"));
        this.setState({ start: newStart });
      }

      renderPickerAutoApplySmartModeDisabled(
        ranges,
        local,
        maxDate,
        descendingYears
      ) {
        let value = `${this.state.start.format(
          "DD/MM/YYYY hh:mm A"
        )} - ${this.state.end.format("DD/MM/YYYY hh:mm A")}`;
        return (
          <div id="DateTimeRangeContainerSmartModeDisabled">
            <DateTimeRangeContainer
              ranges={ranges}
              start={this.state.start}
              end={this.state.end}
              local={local}
              maxDate={maxDate}
              applyCallback={this.applyCallback}
              rangeCallback={this.rangeCallback}
              autoApply
              descendingYears={descendingYears}
              years={[2010, 2020]}
            >
              <FormControl
                id="formControlsTextB"
                type="text"
                label="Text"
                placeholder="Enter text"
                style={{ cursor: "pointer", width:'70%' }}
                value={value}
              />
            </DateTimeRangeContainer>
            <div onClick={this.onClick}>
            </div>
            <br />
          </div>
        );
      }
      render() {
        let now = new Date();
        let start = moment(
          new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0)
        );
        let end = moment(start)
          .add(1, "days")
          .subtract(1, "seconds");
        let ranges = {
          "Today Only": [moment(start), moment(end)],
          "Yesterday Only": [
            moment(start).subtract(1, "days"),
            moment(end).subtract(1, "days")
          ],
          "3 Days": [moment(start).subtract(3, "days"), moment(end)],
          "5 Days": [moment(start).subtract(5, "days"), moment(end)],
          "1 Week": [moment(start).subtract(7, "days"), moment(end)],
          "2 Weeks": [moment(start).subtract(14, "days"), moment(end)],
          "1 Month": [moment(start).subtract(1, "months"), moment(end)],
          "1st August 18": [
            moment("2018-08-01 00:00:00"),
            moment("2018-08-02 23:59:59")
          ],
          "1 Year": [moment(start).subtract(1, "years"), moment(end)]
        };
        let local = {
          format: "DD-MM-YYYY HH:mm",
          sundayFirst: false
        };
        let maxDate = moment(end).add(24, "hour");
        return (
          <div className="container">
            {this.renderPickerAutoApplySmartModeDisabled(
              ranges,
              local,
              maxDate,
              true
            )}
          </div>
        );
      }
}

export default DatePicker;