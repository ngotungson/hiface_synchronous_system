import React from "react";
import axios from 'axios';
import { MDBContainer, MDBRow, MDBCol } from 'mdbreact';
import { Button, ProgressBar } from 'react-bootstrap';
// import ProgressBar from 'react-bootstrap/ProgressBar'
import ReactTagInput from "@pathofdev/react-tag-input";
import "@pathofdev/react-tag-input/build/index.css";

import './hf_sync.css';

class Sync extends React.Component {
  state = {
      serverIP: this.props.userInfoFromParent.serverIP,
      username: this.props.userInfoFromParent.username,
      password: this.props.userInfoFromParent.password,
      url: [],
      libs: [],
      isSyncStart: false,
      message: '',
      syncData: {
        total: 0,
        curent: 0
      }
  }

  

  config = () =>  {
    axios.get('http://192.168.51.28:8031/information/')
    .then(res => {
      this.setState({...res.data});
    });
  }

  // variable i, l used for traverse url and libs arrays
  l = 0
  i = 0

  runProgressBar = async (input_url, input_lib) => {
    this.setState({message: `${input_url} synchronizing....`})
    let tmp1 = []
    let tmp2 = []
    tmp1.push(input_url)
    tmp2.push(input_lib)
    await axios.post('http://192.168.51.28:8031/synchronize/', {
        serverIP: this.state.serverIP,
        username: this.state.username,
        password: this.state.password,
        url: tmp1,
        libs: tmp2
      })
      .then(res => {
        this.setState({message:`${input_url} Completed`});
        console.log(this.state.message)
        this.i++
        
        if (this.i<this.l) {
          console.log('De quy')
          this.runOnce = true
          // this.setState({isSyncStart: true})
          this.runProgressBar(this.state.url[this.i], this.state.libs[this.i])
          // console.log('does this run after send next POST req')
          
        } else {
          this.i = 0
          this.setState({isSyncStart: false})
          this.setState({message: 'All Done'})
          // make sure the progress bar is all full when the sync process is done
          if (this.state.message === 'All Done') {
            let tmp = this.state.syncData.total
            this.setState({syncData: {total:tmp, curent:tmp}})
          }
        }
      })
      .catch(err => {
        console.log('runProcessBar: ' + err)       
      });
  } 



  sync = () =>  {
 
    this.setState({isSyncStart: true})

    this.l = this.state.url.length

    // console.log('count l: ' + this.l)
    this.runProgressBar(this.state.url[0], this.state.libs[0]);
    
  }

  setArrOfServerIP = (newTags) => {
    this.setState({url: newTags})
  }

  setArrOfLibraries = (newTags) => {
    this.setState({libs: newTags})

  }

  intervalID = 0;

 
  componentDidMount() {
    this.config()

  }

  render() {
    // console.log('Rendering: ')
    console.log( this.state.syncData)
    return (
        <MDBContainer>
            <MDBRow>
                <MDBCol md="6">
                <form>
                    <label className="grey-text">
                      List Of Server IP
                    </label>
                  
                    <ReactTagInput
                      tags={this.state.url} 
                      onChange={this.setArrOfServerIP}
                    /><br />
                    <label className="grey-text">
                      Libraries
                    </label>
                  
                    <ReactTagInput
                      tags={this.state.libs} 
                      onChange={this.setArrOfLibraries}
                    />

                    <div className="text-center mt-4">
                      
                        <ProgressBar striped variant="success"
                          now={this.state.syncData.curent*100/this.state.syncData.total}
                          label={`${this.state.syncData.curent}/${this.state.syncData.total}`} 
                        />
                        <Button variant='success' onClick={this.sync}>SYNC</Button>
                        
                        <p style={{color:'#4CAF50'}}>{this.state.message}</p>
                    </div>
                </form>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    );
  }

  runOnce = true
  getSynProcessState = () => {
    if (this.state.isSyncStart  && this.runOnce) {
      this.runOnce = false
    
      this.intervalID = setInterval(() => { 
        axios.get('http://192.168.51.28:8031/process/')
        .then(res => {
          this.setState({syncData: res.data})
        });
      },1000);
    } 
    if (this.state.isSyncStart === false) {
      clearInterval(this.intervalID)
    }
  }

  componentDidUpdate() {
    this.getSynProcessState()
  }


}

export default Sync;