import React from "react";
import { MDBContainer, MDBRow, MDBCol } from 'mdbreact';
import { Button } from 'react-bootstrap';
import axios from 'axios';

import './hf_config.css';



class Config extends React.Component {
  state = {
    serverIP: '',
    username: '',
    password: '',
    saveStatus: '',

  }

  handleChange = event => {
    const value = event.target.value;
    this.setState({
      ...this.state,
      [event.target.name]: value
    });
  }


  save = () => {
    this.setState({saveStatus: 'Saving....'})
    axios.post('http://192.168.51.28:8031/getinf/', {
          serverIP: this.state.serverIP, 
          username: this.state.username, 
          password: this.state.password,
        }
    ).then(res => {
      this.setState({saveStatus: ''})
      this.config()
    })
  }

  config = () =>  {
    axios.get('http://192.168.51.28:8031/information/')
    .then(res => {
      this.setState({...res.data});
    });
  }

  componentDidMount() {
    this.config()
  }

  

  render() {
    return (
      <MDBContainer>
          <MDBRow>
            <MDBCol md="6">
              <form>
                {/* <p className="h4 text-center mb-4">Sign up</p> */}
                <label className="grey-text">
                  Server IP
                </label>
                <input type="text" value={this.state.serverIP} name="serverIP" className="form-control" onChange={this.handleChange}/>
                <br />
                <label className="grey-text">
                  Username
                </label>
                <input type="text" value={this.state.username} name="username" className="form-control" onChange={this.handleChange}/>
                <br />
                <label className="grey-text">
                  Password
                </label>
                <input  type="text" value={this.state.password} name="password" className="form-control" onChange={this.handleChange}/><br />
                
                <div className="text-center mt-4">
                {/* <Button variant='success' onClick={this.save}>CONFIG</Button> */}
                  <Button variant='success' onClick={this.save}>SAVE</Button>
                  <p style={{color:'#4CAF50'}}>{this.state.saveStatus}</p>
                </div>
              </form>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      
    );
  }
}

export default Config;