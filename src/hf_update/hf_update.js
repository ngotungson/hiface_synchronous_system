import React from "react";
import { Button } from 'react-bootstrap';
import { MDBContainer, MDBRow, MDBCol } from 'mdbreact';
import axios from 'axios';

import '../hf_export_report/hf_export_report.css';



class Update extends React.Component {
  state ={
    updateStatus: '',
    // failStatus:'',
    show: true
  }

  click = () => {
    this.setState({failStatus: ''})
    
    let temp = document.getElementById('updateFile');
    let extension = temp.value.split('.').slice(-1)[0] ;
    console.log(temp.value)
    console.log(extension)
    if (extension !== 'xlsx') {
      this.setState({failStatus: 'Please upload again', show:false})
      return ;
    }
    this.setState({updateStatus: 'Updating....'})
    let formData = new FormData();
    formData.append('file', temp.files[0])
    formData.append('serverIP', this.props.userInfoFromParent.serverIP)
    formData.append('username', this.props.userInfoFromParent.username)
    formData.append('password', this.props.userInfoFromParent.password)

    fetch('/update/', {
        mode: 'no-cors',
        method: 'post',
        body: formData
    })
    .then(res => {  
      // console.log(res)
      this.setState({updateStatus: ''})
    })
    .catch(err => console.log('error', err))
  }
  

  config = event =>  {
    axios.get('/information/')
    .then(res => {
      this.setState({...res.data});
    });
  }

  componentDidMount() {
    this.config()
  }

  render() {
    return (
      <MDBContainer>
            <MDBRow>
              <MDBCol md="6">
                <form>
                    <label className="grey-text">Custom File upload</label>
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">Upload</span>
                            </div>
                            <div className="custom-file">
                                <input type="file" id="updateFile"></input>
                                
                            </div>
                        </div>
                    </div>
                    <div className="text-center mt-4">
                        <Button variant='success' onClick={this.click} disabled>UPDATE</Button>
                        <p style={{color:'#FF0000'}}>{this.state.failStatus}</p>
                        <p  style={{color:'#4CAF50'}} >{this.state.updateStatus}</p>
                    </div>
                </form>
              </MDBCol>
            </MDBRow>
          </MDBContainer>
    );
  }
}

export default Update;