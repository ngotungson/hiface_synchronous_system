import React from "react";
import {
  BrowserRouter as Router,
  Link,
  Switch,
  Route,
} from "react-router-dom";

import axios from 'axios';

// Be sure to include styles at some point, probably during your bootstraping
import '@trendmicro/react-sidenav/dist/react-sidenav.css';

import ExportReport from './hf_export_report/hf_export_report';
import Update from './hf_update/hf_update';
import Config from './hf_config/hf_config';
import Sync from './hf_sync/hf_sync';
import './App.css';


class App extends React.Component {
  state = {
    serverIP: '',
    username: '',
    password: '',
    syncPageData: {
      total: 0,
      current: 0
    }
  }

  getUserInfo = userInfo => {
    this.setState({...userInfo})
  }

  config = () =>  {
    axios.get('/information/')
    .then(res => {
      this.setState({...res.data});
    });
  }

  getDataFromSyncPage = data => {
    console.log(data)
    this.setState({syncPageData: data})
  }

  componentDidMount() {
    this.config()
  }

  render() {
    return (
      <div>
        <Router>
        <div className='body'>
            <Switch>
              <Route path="/export">
                <ExportReport userInfoFromParent={this.state}/>
              </Route>
              <Route path="/update">
                <Update userInfoFromParent={this.state} />
              </Route>
              <Route path="/config">
                <Config userInfoFromParent={this.state} />
              </Route>
              <Route path="/sync">
                <Sync userInfoFromParent={this.state} onSyncProgress={this.getDataFromSyncPage}/>
              </Route>
            </Switch>
          </div>
        <div className='menu'>
          <ul className='metismenu-container'>
            <li className='metismenu-item'>
              <div className='metismenu-link'>
                <a href='/'><img src='static/logo.png' alt='HIFACE' /></a>
              </div>
            </li>
            <li className='metismenu-item'>
              <div className='metismenu-link'>
                <i className="metismenu-icon fa fa-cloud-download "></i>
                <Link style={{color: '#ddd'}} to="/export">Export Report</Link>
              </div>
            </li>
            <li className='metismenu-item'>
              <div className='metismenu-link'>  
                <i className="metismenu-icon fa fa-file-excel-o"></i>
                <Link style={{color: '#ddd'}} to="/update">Update</Link>
              </div>
              
            </li>
            <li className='metismenu-item'>
              <div className='metismenu-link'>
                <i className="metismenu-icon fa fa-cogs"></i>
                <Link style={{color: '#ddd'}} to="/config">Config</Link>
              </div>
            </li>
            <li className='metismenu-item'>
              <div className='metismenu-link'>
                <i className="metismenu-icon fa fa-superpowers"></i>
                <Link style={{color: '#ddd'}} to="/sync">Sync</Link>
              </div>
            </li>
          </ul>
         
        </div>
      </Router>

      
      </div>
    );
  }
}


export default App;